import { useState } from 'react'
import reactLogo from './assets/react.svg'
import Contact from './components/Contact'
import './App.css'

function App() {
  return (
    <div className="App">
      <Contact name="vincent" firstName="romand" online={true} />
      <Contact name="squale" firstName="requin" online={false} />
      <Contact name="lucifer" firstName="morningstar" online={true} />
      <Contact name="julien" firstName="paul" online={false} />
    </div>
  )
}

export default App
