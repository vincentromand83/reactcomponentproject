import React from 'react'

export default function contact(props) {
    return (
        <div>{props.name} {props.firstName} {props.online ? "online" : "offline"}</div>
    )
}
